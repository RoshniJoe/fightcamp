//
//  Extras.swift
//  FightCamp-Homework
//
//  Created by Roshni Varghese on 2020-11-15.
//  Copyright © 2020 Alexandre Marcotte. All rights reserved.
//

import UIKit

//MARK: - Extension for UIView to add multiple subviews programmatically

extension UIView {
    func addSubviews(views: [UIView], translatesAutoresizingMaskIntoConstraints: Bool?, in superView: UIView?){
        views.forEach {
            if let translatesAutoresizingMaskIntoConstraints = translatesAutoresizingMaskIntoConstraints {
                $0.translatesAutoresizingMaskIntoConstraints = translatesAutoresizingMaskIntoConstraints
            }
            if let superView = superView{
                superView.addSubview($0)
            }
        }
    }
}

//MARK: - Extension for UIImage to load image from URL

extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }
        
        do {
            self.init(data: try Data(contentsOf: url))
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
}
