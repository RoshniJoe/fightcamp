//
//  PhotoCollectionViewCell.swift
//  FightCamp-Homework
//
//  Created by Roshni Varghese on 2020-11-15.
//  Copyright © 2020 Alexandre Marcotte. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - variables and constants
    
    var imagePreview: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = .thumbnailRadius
        imageView.layer.borderWidth = .thumbnailBorderWidth
        imageView.layer.borderColor = UIColor.thumbnailBorder(selected: false).cgColor
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    //MARK: - Method to update the UI to show which of the 4 images' preview is shown
    
    func toggleSelection(selected: Bool){
        imagePreview.layer.borderColor = UIColor.thumbnailBorder(selected: selected).cgColor
    }
    
    //MARK: - Setup the view with autolayout & constraints
    
    func addViews(){
        backgroundColor = UIColor.primaryBackground
        
        addSubviews(views: [imagePreview], translatesAutoresizingMaskIntoConstraints: false, in: contentView)
        
        NSLayoutConstraint.activate([
            
            imagePreview.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            imagePreview.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            imagePreview.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            imagePreview.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor)
            
        ])
    }
}
