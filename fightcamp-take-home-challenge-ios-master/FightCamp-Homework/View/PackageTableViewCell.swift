//
//  PackageTableViewCell.swift
//  FightCamp-Homework
//
//  Created by Roshni Varghese on 2020-11-13.
//  Copyright © 2020 Alexandre Marcotte. All rights reserved.
//

import UIKit

//MARK: - Delegate method declaration to update the main image preview on selection from the 4 images provided

protocol UpdateMainImageDelegate : class {
    func updateMainImagePreviewUponSelection(collectionItemIndex: Int, tableRowIndex: Int)
}

//MARK: - Struct to pass data while setting up the tableview cell

struct PackageTableViewCellConfiguration {
    let packageTitle: String
    let packageSubtitle: String
    let imageUrls: [String]
    let includedDetails: [String]
    let excludedDetails: [String]
    let priceTitle: String
    let priceValue: String
    let buttonTitle: String
    
}

class PackageTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        addViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addViews()
    }
    
    //MARK: - variables and constants
    
    public var photoPreviews: UICollectionView!
    private var photoUrls: [String]?
    
    // photo size to fit 4 items in a row with equal sides
    let kPhotoSize = (UIScreen.main.bounds.width - (.packageSpacing * 4 + .thumbnailSpacing * 3))/4
    private let cellIdentifier = "PhotoCell"
    private var selectedPhotoIndex = 0
    
    weak var delegate : UpdateMainImageDelegate?
    
    private lazy var packageView: UIView = {
        let view = UIView()
        view.backgroundColor = .primaryBackground
        view.layer.cornerRadius = .packageRadius
        view.layer.masksToBounds = true
        return view
    }()
    
    private lazy var packageTitleLabel: UILabel = {
        let title = UILabel()
        title.textColor = .brandRed
        title.font = UIFont.title
        title.numberOfLines = 1
        return title
    }()
    
    private lazy var packageSubtitleLabel: UILabel = {
        let title = UILabel()
        title.textColor = .label
        title.font = UIFont.body
        title.numberOfLines = 0
        title.lineBreakMode = .byWordWrapping
        return title
    }()
    
    public lazy var thumbnailImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = CGFloat.thumbnailRadius
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var galleryView: UIView = {
        let galleryView = UIView()
        return galleryView
    }()
    
    private lazy var packageIncludedDetails: UILabel = {
        let label = UILabel()
        label.font = UIFont.body
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = .label
        return label
    }()
    
    private lazy var packageExcludedDetails: UILabel = {
        let label = UILabel()
        label.font = UIFont.body
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = .disabledLabel
        return label
    }()
    
    
    private lazy var priceTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.body
        label.textColor = UIColor.label
        label.numberOfLines = 1
        label.textAlignment = .center
        return label
    }()
    
    private lazy var priceValueLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.price
        label.textColor = UIColor.label
        label.numberOfLines = 1
        label.textAlignment = .center
        return label
    }()
    
    private lazy var viewPackageButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.button
        button.setTitleColor(.buttonTitle, for: .normal)
        button.backgroundColor = .buttonBackground
        button.layer.cornerRadius = CGFloat.buttonRadius
        return button
    }()
    
    //MARK: - Setup the view with autolayout & constraints
    
    func addViews(){
        
        self.backgroundColor = .secondaryBackground
        
        // setup the collectionview to display the 4 image previews
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        photoPreviews = UICollectionView(frame: galleryView.bounds, collectionViewLayout: layout)
        photoPreviews.dataSource = self
        photoPreviews.delegate = self
        photoPreviews.collectionViewLayout = layout
        photoPreviews.showsHorizontalScrollIndicator = false
        photoPreviews.isPagingEnabled = true
        photoPreviews.backgroundColor = UIColor.primaryBackground
        photoPreviews.register(PhotoCollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        
        contentView.addSubviews(views: [packageView], translatesAutoresizingMaskIntoConstraints: false, in: contentView)
        
        packageView.addSubviews(views: [packageTitleLabel, packageSubtitleLabel, thumbnailImageView, galleryView, packageIncludedDetails, packageExcludedDetails, priceTitleLabel, priceValueLabel, viewPackageButton], translatesAutoresizingMaskIntoConstraints: false, in: packageView)
        
        
        NSLayoutConstraint.activate([
            
            packageView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: .packageSpacing),
            packageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: .packageSpacing),
            packageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -(.packageSpacing)),
            packageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            
            
            packageTitleLabel.topAnchor.constraint(equalTo: packageView.topAnchor, constant: .packageSpacing),
            packageTitleLabel.leadingAnchor.constraint(equalTo: packageView.leadingAnchor, constant: .packageSpacing),
            packageTitleLabel.trailingAnchor.constraint(equalTo: packageView.trailingAnchor, constant: -(.packageSpacing)),
            
            packageSubtitleLabel.topAnchor.constraint(equalTo: packageTitleLabel.bottomAnchor, constant: .packageSpacing),
            packageSubtitleLabel.leadingAnchor.constraint(equalTo: packageView.leadingAnchor, constant: .packageSpacing),
            packageSubtitleLabel.trailingAnchor.constraint(equalTo: packageView.trailingAnchor, constant: -(.packageSpacing)),
            
            thumbnailImageView.topAnchor.constraint(equalTo: packageSubtitleLabel.bottomAnchor, constant: .packageSpacing),
            thumbnailImageView.leadingAnchor.constraint(equalTo: packageView.leadingAnchor, constant: .packageSpacing),
            thumbnailImageView.trailingAnchor.constraint(equalTo: packageView.trailingAnchor, constant: -(.packageSpacing)),
            thumbnailImageView.heightAnchor.constraint(equalToConstant: .thumbnailHeight),
            
            galleryView.topAnchor.constraint(equalTo: thumbnailImageView.bottomAnchor, constant: .thumbnailSpacing),
            galleryView.leadingAnchor.constraint(equalTo: packageView.leadingAnchor, constant: .packageSpacing),
            galleryView.trailingAnchor.constraint(equalTo: packageView.trailingAnchor, constant: -(.packageSpacing)),
            galleryView.heightAnchor.constraint(equalToConstant: kPhotoSize),
            
            packageIncludedDetails.topAnchor.constraint(equalTo: galleryView.bottomAnchor, constant: .packageSpacing),
            packageIncludedDetails.leadingAnchor.constraint(equalTo: packageView.leadingAnchor, constant: .packageSpacing),
            packageIncludedDetails.trailingAnchor.constraint(equalTo: packageView.trailingAnchor, constant: -(.packageSpacing)),
            
            packageExcludedDetails.topAnchor.constraint(equalTo: packageIncludedDetails.bottomAnchor, constant: .thumbnailSpacing),
            packageExcludedDetails.leadingAnchor.constraint(equalTo: packageView.leadingAnchor, constant: .packageSpacing),
            packageExcludedDetails.trailingAnchor.constraint(equalTo: packageView.trailingAnchor, constant: -(.packageSpacing)),
            
            priceTitleLabel.topAnchor.constraint(equalTo: packageExcludedDetails.bottomAnchor, constant: .packageSpacing),
            priceTitleLabel.leadingAnchor.constraint(equalTo: packageView.leadingAnchor, constant: .packageSpacing),
            priceTitleLabel.trailingAnchor.constraint(equalTo: packageView.trailingAnchor, constant: -(.packageSpacing)),
            
            priceValueLabel.topAnchor.constraint(equalTo: priceTitleLabel.bottomAnchor, constant: .thumbnailRadius),
            priceValueLabel.leadingAnchor.constraint(equalTo: packageView.leadingAnchor, constant: .packageSpacing),
            priceValueLabel.trailingAnchor.constraint(equalTo:packageView.trailingAnchor, constant: -(.packageSpacing)),
            
            viewPackageButton.topAnchor.constraint(equalTo: priceValueLabel.bottomAnchor, constant: .packageSpacing),
            viewPackageButton.leadingAnchor.constraint(equalTo: packageView.leadingAnchor, constant: .packageSpacing),
            viewPackageButton.trailingAnchor.constraint(equalTo: packageView.trailingAnchor, constant: -(.packageSpacing)),
            viewPackageButton.heightAnchor.constraint(equalToConstant: .buttonHeight),
            viewPackageButton.bottomAnchor.constraint(equalTo: packageView.bottomAnchor, constant: -(.packageSpacing))
            
        ])
        
        galleryView.addSubviews(views: [photoPreviews], translatesAutoresizingMaskIntoConstraints: false, in: galleryView)
        
        NSLayoutConstraint.activate([
            
            photoPreviews.topAnchor.constraint(equalTo: galleryView.topAnchor),
            photoPreviews.leadingAnchor.constraint(equalTo: galleryView.leadingAnchor),
            photoPreviews.trailingAnchor.constraint(equalTo: galleryView.trailingAnchor),
            photoPreviews.bottomAnchor.constraint(equalTo: galleryView.bottomAnchor)
            
        ])
        
    }
    
    //MARK:- Method to help configure the cell showing the package details using the configuration struct
    
    func configurePackageCell(with configuration: PackageTableViewCellConfiguration){
        packageTitleLabel.text = configuration.packageTitle.uppercased()
        packageSubtitleLabel.text = configuration.packageSubtitle.capitalized
        photoUrls = configuration.imageUrls
        thumbnailImageView.image = UIImage(url: URL(string: configuration.imageUrls[selectedPhotoIndex])) ?? #imageLiteral(resourceName: "ConnectionError")
        
        let includedAttributedString = NSMutableAttributedString(string: configuration.includedDetails.joined(separator: "\n").capitalized)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = .thumbnailSpacing
        includedAttributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, includedAttributedString.length))
        packageIncludedDetails.attributedText = includedAttributedString
        
        let excludedAttributedString = NSMutableAttributedString(string: configuration.excludedDetails.joined(separator: "\n").capitalized)
        excludedAttributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, excludedAttributedString.length))
        excludedAttributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.disabledLabel, range: NSMakeRange(0, excludedAttributedString.length))
        excludedAttributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, excludedAttributedString.length))
        packageExcludedDetails.attributedText = excludedAttributedString
        
        priceTitleLabel.text = configuration.priceTitle.capitalized
        priceValueLabel.text = configuration.priceValue.capitalized
        viewPackageButton.setTitle(configuration.buttonTitle.capitalized, for: .normal)
    }
    
}

//MARK: - Collectionview protocols

extension PackageTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoUrls?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PhotoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! PhotoCollectionViewCell
        if let urlString = photoUrls?[indexPath.item] {
            cell.imagePreview.image = UIImage(url: URL(string: urlString)) ?? #imageLiteral(resourceName: "ConnectionError")
        }
        cell.toggleSelection(selected: indexPath.item == selectedPhotoIndex)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: kPhotoSize, height: kPhotoSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat.thumbnailSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedPhotoIndex = indexPath.item
        photoPreviews.reloadData()
        delegate?.updateMainImagePreviewUponSelection(collectionItemIndex: indexPath.item, tableRowIndex: collectionView.tag)
    }
    
}
