//
//  Package.swift
//  FightCamp-Homework
//
//  Created by Roshni Varghese on 2020-11-15.
//  Copyright © 2020 Alexandre Marcotte. All rights reserved.
//

import Foundation

//MARK: - Using the codable protocol, decode the JSON data into an instance of 'Package'

public struct Package : Decodable {
    let title : String?
    let desc : String?
    let thumbnail_urls : [String]?
    let included : [String]?
    let excluded : [String]?
    let payment : String?
    let price : Int?
    let action : String?
}
