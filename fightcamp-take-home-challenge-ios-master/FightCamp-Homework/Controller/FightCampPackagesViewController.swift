//
//  FightCampPackagesViewController.swift
//  FightCamp-Homework
//
//  Created by Roshni Varghese on 2020-11-13.
//  Copyright © 2020 Alexandre Marcotte. All rights reserved.
//

import UIKit

class FightCampPackagesViewController: UIViewController {
    
    //MARK: - variables and constants
    
    private let tableView = UITableView()
    private let reuseIdentifier = "PackageCell"
    
    var safeArea: UILayoutGuide!
    
    var viewModel = PackageViewModel()
    
    //MARK: - View hierarchy
    
    override func loadView() {
        super.loadView()
        safeArea = view.layoutMarginsGuide
        view.backgroundColor = .secondaryBackground
        setupTableView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        loadJson()
    }
    
    //MARK: - Configure the UI
    
    func setupTableView() {
        view.addSubview(tableView)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = UIColor.secondaryBackground
        tableView.register(PackageTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
    }
    
    //MARK: - Load data from json file using Decodable
    
    func loadJson(){
        PackageViewModel().loadJson() { result in
            switch result {
            case .success(let packages):
                self.viewModel.packages = packages
                self.tableView.reloadData()
            case .failure(_):
                let alertController = UIAlertController(title: "Error", message: "Invalid or No Json data", preferredStyle: .alert)
                let action = UIAlertAction(title: "ok", style: .default, handler: nil)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}

//MARK: - Tableview protocols

extension FightCampPackagesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let packages = viewModel.packages else { return 0 }
        return packages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! PackageTableViewCell
        cell.delegate = self
        cell.photoPreviews.tag = indexPath.row
        cell.layer.cornerRadius = .packageRadius
        cell.layer.masksToBounds = true
        let package = viewModel.packages?[indexPath.row]
        let configuration = PackageTableViewCellConfiguration(packageTitle: package?.title ?? "",
                                                              packageSubtitle: package?.desc ?? "",
                                                              imageUrls: package?.thumbnail_urls ?? [],
                                                              includedDetails: package?.included ?? [],
                                                              excludedDetails: package?.excluded ?? [],
                                                              priceTitle: package?.payment ?? "",
                                                              priceValue: "$ \(package?.price ?? 0)",
                                                              buttonTitle: package?.action ?? "")
        cell.configurePackageCell(with: configuration)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: - Delegate method implemetation to update the main image preview on selection from the 4 images provided

extension FightCampPackagesViewController: UpdateMainImageDelegate {
    func updateMainImagePreviewUponSelection(collectionItemIndex: Int, tableRowIndex: Int) {
        let cell = tableView.cellForRow(at: IndexPath(row: tableRowIndex, section: 0)) as? PackageTableViewCell
        let urlString = viewModel.packages?[tableRowIndex].thumbnail_urls?[collectionItemIndex]
        cell?.thumbnailImageView.image = UIImage(url: URL(string: urlString ?? "")) ?? #imageLiteral(resourceName: "ConnectionError")
    }
}
