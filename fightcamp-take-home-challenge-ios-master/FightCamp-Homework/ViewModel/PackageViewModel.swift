//
//  PackageViewModel.swift
//  FightCamp-Homework
//
//  Created by Roshni Varghese on 2020-11-15.
//  Copyright © 2020 Alexandre Marcotte. All rights reserved.
//

import UIKit

class PackageViewModel: NSObject {
    
    var packages : [Package]?
    
    //MARK: - Load data from json file using Decodable
    
    func loadJson(completionHandler: @escaping (Result<[Package], DataError>) -> Void){
        if let url = Bundle.main.url(forResource: "packages", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let packages = try decoder.decode([Package].self, from: data)
                completionHandler(.success(packages))
            } catch {
                completionHandler(.failure(.badData))
            }
        }
    }
}

//MARK: - Enum to specify different errors if applicable

public enum DataError: Error {
    case badData
}
