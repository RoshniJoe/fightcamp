## FightCamp take home (iOS) 🥊

## Summary of the reasoning behind the technical decisions.

1. The architecture followed is MVVM.
2. The data to be populated in the UI is mapped to a struct of type Decodable, for easiness in getting the individual properties rather than retrieving data from arrays and dictionaries using keys.
3. Usage of the Result type : turning possible result of parsing JSON into two separate states, by using an enum containing a case for each state — one for success and one for failure.
4. The UI is created programmatically which makes it easier to resolve conflicts when committing to a repository and you can see what is being built rather than being hidden in a separate xib or storyboard file.
5. Show list of packages as per data from json file with the help of the 'Package' object for easy data access.
6. Usage of tableview to show list of packages which scrolls vertically accomodating as many items present in the json file.
7. Usage of collectionview to show the list of images and enable selection among them, to be previewed.
8. Pass relevant indices to show preview on selecting one out of the 4 images using protocol.
9. Usage of closures to handle values of call backs in a cleaner way.
10. Usage of extensions (of UIView and UIImage) to avoid redundant and repetitive code blocks.




